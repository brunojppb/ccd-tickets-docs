# CCD - Sistema de Requisições de busca de dados

Documentação das funcionalidades, roadmap de implementação e o que ja está pronto para entrega.

## Funcionalide principal

A principal funcionalidade do sistema é melhorar a eficiência da comunicação do Agente em campo com o CCD, facilitando a busca de informações em todos os dados disponíveis. O sistema gerenciará uma fila de pedidos, onde os usuários responsáveis no CCD irão cadastrar novos pedidos, bem como responder a pedidos existentes na fila.  
Abaixo temos os diagramas de sequência da funcionalidade principal da aplicação.

### Respondendo a uma novo pedido
```mermaid
sequenceDiagram
    participant A as Agente em Campo
    participant B as Agente CCD
    participant C as Sistema De Req.
    A->>+B: Solicita info sobre indivíduo
    B->>C: Cadastra solicitação
    B-->>B: Busca info em vários bancos de dados
    B->>C: Atualiza solicitação com resultado da busca
    B-->>-A: Entrega resultado da busca
```

### Respondendo a pedidos pendentes
```mermaid
sequenceDiagram
    participant A as Agente CCD
    participant B as Sistema De Req.
    participant C as Agente em Campo
    A->>+B: Solicita lista de pedidos pendentes
    B-->>-A: Mostra fila de pedidos
    A->>+B: Seleciona pedido com maior prioridade
    B-->>-A: Returna pedido selecionado
    A-->>A: Realiza busca
    A->>+B: atualiza pedido atual
    B-->>-A: returna pedido atualizado
    A->>C: Entrega resultado da busca
```

# Funcionalidades
- [ ] Login do usuário operador
- [ ] Manter de pedidos
- [ ] Listagem de pedidos em espera (fila)
- [ ] Filtro de pedidos por status (na fila, em atendimento, finalizado ou resolvido)
- [ ] Adicionar responsável por responder pedido em atendimento
- [ ] Responder pedido
- [ ] Lincar pedido com ocorrência cadastrada (Provável API com SCAIC?)
- [ ] Enriquecer ocorrência já cadastrada com mais informações como fotos, vídeos e áudios. (Provavel API com SCAIC?)

# Modelo de Dados

Modelação inicial das entidades salvas em banco de dados.
> **Atenção**: Essa é a ideia inicial. Muito provavelmente teremos muito mais modelos e atributos. Por favor, contribua com a evolução desse documento deixando comentários ou abrindo issues no repositório.

![Modelo de dados](images/model_diagram.jpg)